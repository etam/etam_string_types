#include <boost/test/unit_test.hpp>

#include <etam_types/base64string.hpp>

#include <stdexcept>

#include <etam_types/bytes.hpp>
#include <etam_types/hexstring.hpp>
#include <etam_types/nibbles.hpp>

BOOST_AUTO_TEST_SUITE( base64string )

BOOST_AUTO_TEST_CASE( emtpy )
{
    BOOST_CHECK_NO_THROW( etam::Base64String{""} );
}

BOOST_AUTO_TEST_CASE( from_bytes )
{
    const auto table = std::vector<std::pair<etam::Bytes, etam::Base64String>>{
        { {0x49,0x27,0x6d,0x20,0x6b,0x69,0x6c,0x6c,0x69,0x6e,0x67,0x20,0x79,0x6f,0x75,0x72,0x20,0x62,0x72,0x61,0x69,0x6e,0x20,0x6c,0x69,0x6b,0x65,0x20,0x61,0x20,0x70,0x6f,0x69,0x73,0x6f,0x6e,0x6f,0x75,0x73,0x20,0x6d,0x75,0x73,0x68,0x72,0x6f,0x6f,0x6d}, "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t" },
        { {0x4d,0x61,0x6e}, "TWFu" },
        { {0x4d,0x61}, "TWE=" },
        { {0x4d}, "TQ==" },
        { {}, "" }
    };
    for (const auto& pair : table) {
        const auto result = etam::Base64String::from_bytes(pair.first);
        BOOST_CHECK_EQUAL( result, pair.second );
    }
}

BOOST_AUTO_TEST_CASE( from_hex )
{
    const auto hex = etam::HexString{"49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"};
    const auto expected_result = etam::Base64String{"SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"};
    const auto result = etam::Base64String::from_hex(hex);
    BOOST_CHECK_EQUAL( result, expected_result );
}

BOOST_AUTO_TEST_CASE( from_nibbles )
{
    const auto nibbles = etam::Nibbles{0x4,0x9,0x2,0x7,0x6,0xd,0x2,0x0,0x6};
    const auto expected_result = etam::Base64String{"SSdtIG"};
    const auto result = etam::Base64String::from_nibbles(nibbles);
    BOOST_CHECK_EQUAL( result, expected_result );
}

BOOST_AUTO_TEST_CASE( padding )
{
    BOOST_CHECK_NO_THROW( etam::Base64String("cGxlYXN1cmUu") );
    BOOST_CHECK_NO_THROW( etam::Base64String("bGVhc3VyZS4=") );
    BOOST_CHECK_NO_THROW( etam::Base64String("bGVhc3VyZS4") );
    BOOST_CHECK_NO_THROW( etam::Base64String("ZWFzdXJlLg==") );
    BOOST_CHECK_NO_THROW( etam::Base64String("ZWFzdXJlLg") );
    BOOST_CHECK_THROW( etam::Base64String("cGxlYXN1c"), std::invalid_argument );
    BOOST_CHECK_THROW( etam::Base64String("cGxlYXN1cm=u"), std::invalid_argument );
    BOOST_CHECK_THROW( etam::Base64String("=GxlYXN1cmU"), std::invalid_argument );
    BOOST_CHECK_THROW( etam::Base64String("cGxlY=N1cm"), std::invalid_argument );
}

BOOST_AUTO_TEST_SUITE_END()
