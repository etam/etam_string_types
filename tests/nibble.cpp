#include <boost/test/unit_test.hpp>

#include <etam_types/nibble.hpp>

#include <vector>

#include <etam_types/hexchar.hpp>

BOOST_AUTO_TEST_SUITE( nibble )

BOOST_AUTO_TEST_CASE( from_hex )
{
    const auto table = std::vector<std::pair<etam::HexChar, etam::Nibble>>{
        {'0', 0},
        {'1', 1},
        {'2', 2},
        {'3', 3},
        {'4', 4},
        {'5', 5},
        {'6', 6},
        {'7', 7},
        {'8', 8},
        {'9', 9},
        {'a',10},
        {'b',11},
        {'c',12},
        {'d',13},
        {'e',14},
        {'f',15},
        {'A',10},
        {'B',11},
        {'C',12},
        {'D',13},
        {'E',14},
        {'F',15}
    };
    for (const auto& pair : table) {
        const auto result = etam::Nibble::from_hex(pair.first);
        BOOST_CHECK_MESSAGE( result == pair.second, "Nibble::from_hex(" << pair.first << ") == " << uint(result) << " != " << uint(pair.second) );
    }
}

BOOST_AUTO_TEST_SUITE_END()
