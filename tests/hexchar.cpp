#include <boost/test/unit_test.hpp>

#include <etam_types/hexchar.hpp>

#include <vector>

#include <etam_types/nibble.hpp>

BOOST_AUTO_TEST_SUITE( hexchar )

BOOST_AUTO_TEST_CASE( from_nibble )
{
    const auto table = std::vector<std::pair<etam::Nibble, etam::HexChar>>{
        { 0, '0'},
        { 1, '1'},
        { 2, '2'},
        { 3, '3'},
        { 4, '4'},
        { 5, '5'},
        { 6, '6'},
        { 7, '7'},
        { 8, '8'},
        { 9, '9'},
        {10, 'a'},
        {11, 'b'},
        {12, 'c'},
        {13, 'd'},
        {14, 'e'},
        {15, 'f'}
    };
    for (const auto& pair : table) {
        const auto result = etam::HexChar::from_nibble(pair.first);
        BOOST_CHECK_EQUAL( result, pair.second );
    }
}

BOOST_AUTO_TEST_SUITE_END()
