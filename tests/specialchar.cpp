#include <boost/test/unit_test.hpp>

#include <stdexcept>

#include <etam_types/detail/specialchar.hpp>

BOOST_AUTO_TEST_SUITE( specialchar )

namespace {

class TestSpecialChar
    : public etam::detail::SpecialChar<TestSpecialChar>
{
private:
    typedef etam::detail::SpecialChar<TestSpecialChar> base_type;
    friend base_type;

    static void throw_if_illegal(const char char_)
    {
        if (char_ == 'a')
            throw std::invalid_argument{"invalid test char"};
    }

public:
    using base_type::base_type;

    static TestSpecialChar create_unchecked(const char char_)
    {
        return base_type::create_unchecked(char_);
    }
};

} // namespace

BOOST_AUTO_TEST_CASE( accept_null_char )
{
    BOOST_CHECK( TestSpecialChar{'\0'} == '\0' );
}

BOOST_AUTO_TEST_CASE( throw_constructor_from_char )
{
    BOOST_CHECK_THROW( TestSpecialChar{'a'}, std::invalid_argument );
}

BOOST_AUTO_TEST_CASE( throw_assignment_from_char )
{
    auto c = TestSpecialChar{};
    BOOST_CHECK_THROW( c = 'a', std::invalid_argument );
}

BOOST_AUTO_TEST_CASE( create_unchecked )
{
    BOOST_CHECK( TestSpecialChar::create_unchecked('a') == 'a' );
}

BOOST_AUTO_TEST_SUITE_END()
