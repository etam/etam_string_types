#include <boost/test/unit_test.hpp>

#include <etam_types/detail/specialstring.hpp>

#include <algorithm>
#include <stdexcept>

#include <etam_types/detail/specialchar.hpp>

BOOST_AUTO_TEST_SUITE( specialstring )

namespace {

class TestSpecialString;

class TestSpecialChar
    : public etam::detail::SpecialChar<TestSpecialChar>
{
private:
    typedef etam::detail::SpecialChar<TestSpecialChar> BaseType;
    friend BaseType;

    friend etam::detail::SpecialString<TestSpecialString, TestSpecialChar>;

    static const TestSpecialChar safe_default;

    static void throw_if_illegal(const char char_)
    {
        if (char_ == 'a')
            throw std::invalid_argument{"invalid test char"};
    }

public:
    using BaseType::BaseType;
};

const TestSpecialChar TestSpecialChar::safe_default{'?'};

class TestSpecialString
    : public etam::detail::SpecialString<TestSpecialString, TestSpecialChar>
{
private:
    typedef etam::detail::SpecialString<TestSpecialString, TestSpecialChar> BaseType;
    friend BaseType;

    template <class CharIt>
    static void throw_if_illegal(CharIt first, CharIt last)
    {
        if (std::any_of(first, last, [](const char c) { return c == 'b'; }))
            throw std::invalid_argument{"invalid test string"};
    }

public:
    using BaseType::BaseType;
};

} // namespace

BOOST_AUTO_TEST_CASE( throw_create_from_string )
{
    BOOST_CHECK_THROW( TestSpecialString{"abcdef"}, std::invalid_argument );
    BOOST_CHECK_THROW( TestSpecialString{"acdef"}, std::invalid_argument );
    BOOST_CHECK_THROW( TestSpecialString{"bcdef"}, std::invalid_argument );
}

BOOST_AUTO_TEST_CASE( to_string )
{
    const auto std_string = std::string{"cdef"};
    const auto special_string = TestSpecialString{std_string};
    const auto std_string_again = special_string.to_string_view();
    BOOST_CHECK_EQUAL( std_string_again, std_string );
}

BOOST_AUTO_TEST_CASE( compare_with_string )
{
    const auto std_string = std::string{"cdef"};
    BOOST_CHECK_EQUAL( TestSpecialString{std_string}, std_string );
    BOOST_CHECK_EQUAL( std_string, TestSpecialString{std_string} );
}

BOOST_AUTO_TEST_SUITE_END()
