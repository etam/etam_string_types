#include <boost/test/unit_test.hpp>

#include <etam_types/base64char.hpp>

#include <vector>

BOOST_AUTO_TEST_SUITE( basechar )

BOOST_AUTO_TEST_CASE( from_byte )
{
    const auto table = std::vector<std::pair<byte, etam::Base64Char>>{
        { 0, 'A'},
        { 1, 'B'},
        { 2, 'C'},
        { 3, 'D'},
        { 4, 'E'},
        { 5, 'F'},
        { 6, 'G'},
        { 7, 'H'},
        { 8, 'I'},
        { 9, 'J'},
        {10, 'K'},
        {11, 'L'},
        {12, 'M'},
        {13, 'N'},
        {14, 'O'},
        {15, 'P'},
        {16, 'Q'},
        {17, 'R'},
        {18, 'S'},
        {19, 'T'},
        {20, 'U'},
        {21, 'V'},
        {22, 'W'},
        {23, 'X'},
        {24, 'Y'},
        {25, 'Z'},
        {26, 'a'},
        {27, 'b'},
        {28, 'c'},
        {29, 'd'},
        {30, 'e'},
        {31, 'f'},
        {32, 'g'},
        {33, 'h'},
        {34, 'i'},
        {35, 'j'},
        {36, 'k'},
        {37, 'l'},
        {38, 'm'},
        {39, 'n'},
        {40, 'o'},
        {41, 'p'},
        {42, 'q'},
        {43, 'r'},
        {44, 's'},
        {45, 't'},
        {46, 'u'},
        {47, 'v'},
        {48, 'w'},
        {49, 'x'},
        {50, 'y'},
        {51, 'z'},
        {52, '0'},
        {53, '1'},
        {54, '2'},
        {55, '3'},
        {56, '4'},
        {57, '5'},
        {58, '6'},
        {59, '7'},
        {60, '8'},
        {61, '9'},
        {62, '+'},
        {63, '/'}
    };
    for (const auto& pair : table) {
        const auto result_from_byte = etam::Base64Char::from_byte(pair.first);
        const auto result_to_byte = pair.second.to_byte();
        BOOST_CHECK_MESSAGE( result_from_byte == pair.second, "Base64Char::from_byte(" << uint(pair.first) << ") == " << uint(result_from_byte) << " != " << uint(pair.second) );
        BOOST_CHECK_MESSAGE( result_to_byte == pair.first, "Base64Char(" << uint(pair.second) << ").to_byte() == " << uint(result_to_byte) << " != " << uint(pair.first) );
    }
    BOOST_CHECK_EQUAL( etam::Base64Char::padding.to_byte(), 0 );
    BOOST_CHECK_THROW( etam::Base64Char::from_byte(64), std::out_of_range );
}

BOOST_AUTO_TEST_SUITE_END()
