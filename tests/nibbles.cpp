#include <boost/test/unit_test.hpp>

#include <etam_types/nibbles.hpp>

#include <sstream>

#include <etam_types/bytes.hpp>
#include <etam_types/hexstring.hpp>

namespace etam {

static
std::ostream& operator<<(std::ostream& ostream, const Nibbles& nibbles)
{
    for (const auto& nibble : nibbles)
        ostream << std::hex << static_cast<unsigned>(nibble);
    return ostream;
}

} // namespace etam

BOOST_AUTO_TEST_SUITE( nibbles )

BOOST_AUTO_TEST_CASE( output )
{
    std::ostringstream stream;
    stream << etam::Nibbles{0x4,0xd,0x6,0x1,0x6,0xE};
    BOOST_CHECK_EQUAL( stream.str(), "4d616e" );
}

BOOST_AUTO_TEST_CASE( from_bytes )
{
    const auto bytes = etam::Bytes{0x4d,0x61,0x6E};
    const auto expected_result = etam::Nibbles{0x4,0xd,0x6,0x1,0x6,0xE};
    const auto result = etam::Nibbles::from_bytes(bytes);
    BOOST_CHECK_EQUAL( result, expected_result );
}

BOOST_AUTO_TEST_CASE( from_hex )
{
    const auto hex = etam::HexString{"4d616E"};
    const auto expected_result = etam::Nibbles{0x4,0xd,0x6,0x1,0x6,0xE};
    const auto result = etam::Nibbles::from_hex(hex);
    BOOST_CHECK_EQUAL( result, expected_result );
}

BOOST_AUTO_TEST_CASE( from_string )
{
    const auto string = std::string{"Man"};
    const auto expected_nibbles = etam::Nibbles{0x4,0xd,0x6,0x1,0x6,0xE};
    const auto result = etam::Nibbles::from_string(string);
    BOOST_CHECK_EQUAL( result, expected_nibbles );
}

BOOST_AUTO_TEST_CASE( to_string )
{
    const auto nibbles = etam::Nibbles{0x4,0xd,0x6,0x1,0x6,0xE};
    const auto expected_string = std::string{"Man"};
    const auto result = nibbles.to_string();
    BOOST_CHECK_EQUAL( result, expected_string );
}

BOOST_AUTO_TEST_SUITE_END()
