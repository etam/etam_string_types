#include <boost/test/unit_test.hpp>

#include <etam_types/bytes.hpp>

#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <etam_types/base64string.hpp>
#include <etam_types/hexstring.hpp>
#include <etam_types/nibbles.hpp>

namespace etam {

static
std::ostream& operator<<(std::ostream& ostream, const Bytes& bytes)
{
    for (const auto& byte : bytes)
        ostream << std::hex << std::setw(2) << std::setfill('0') << static_cast<unsigned>(byte);
    return ostream;
}

} // namespace etam

BOOST_AUTO_TEST_SUITE( bytes )

BOOST_AUTO_TEST_CASE( output )
{
    std::ostringstream stream;
    stream << etam::Bytes{0x12,0x34,0x56,0x78,0x9,0xab,0xcd,0xef};
    BOOST_CHECK_EQUAL( stream.str(), "1234567809abcdef" );
}

BOOST_AUTO_TEST_CASE( from_base64 )
{
    const auto table = std::vector<std::pair<etam::Base64String, etam::Bytes>>{
        { "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t", {0x49,0x27,0x6d,0x20,0x6b,0x69,0x6c,0x6c,0x69,0x6e,0x67,0x20,0x79,0x6f,0x75,0x72,0x20,0x62,0x72,0x61,0x69,0x6e,0x20,0x6c,0x69,0x6b,0x65,0x20,0x61,0x20,0x70,0x6f,0x69,0x73,0x6f,0x6e,0x6f,0x75,0x73,0x20,0x6d,0x75,0x73,0x68,0x72,0x6f,0x6f,0x6d} },
        { "TWFu", {0x4d,0x61,0x6e} },
        { "TWE=", {0x4d,0x61} },
        { "TQ==", {0x4d} },
        { "TWFu", {0x4d,0x61,0x6e} },
        { "TWE", {0x4d,0x61} },
        { "TQ", {0x4d} },
        { "", {} }
    };
    for (const auto& pair : table) {
        const auto result = etam::Bytes::from_base64(pair.first);
        BOOST_CHECK_EQUAL( result, pair.second );
    }
}

BOOST_AUTO_TEST_CASE( from_hex_even )
{
    const auto hexstring = etam::HexString("1234567890abcdef");
    const auto expected_result = etam::Bytes({0x12,0x34,0x56,0x78,0x90,0xab,0xcd,0xef});
    const auto result = etam::Bytes::from_hex(hexstring);
    BOOST_CHECK_EQUAL( result, expected_result );
}

BOOST_AUTO_TEST_CASE( from_hex_odd )
{
    const auto hexstring = etam::HexString("1234567890abcde");
    const auto expected_result = etam::Bytes({0x12,0x34,0x56,0x78,0x90,0xab,0xcd,0xe0});
    const auto result = etam::Bytes::from_hex(hexstring);
    BOOST_CHECK_EQUAL( result, expected_result );
}

BOOST_AUTO_TEST_CASE( from_nibbles_even )
{
    const auto nibbles = etam::Nibbles({0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0xa,0xb,0xc,0xd,0xe,0xf});
    const auto expected_result = etam::Bytes({0x12,0x34,0x56,0x78,0x90,0xab,0xcd,0xef});
    const auto result = etam::Bytes::from_nibbles(nibbles);
    BOOST_CHECK_EQUAL( result, expected_result );
}

BOOST_AUTO_TEST_CASE( from_nibbles_odd )
{
    const auto nibbles = etam::Nibbles({0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0xa,0xb,0xc,0xd,0xe});
    const auto expected_result = etam::Bytes({0x12,0x34,0x56,0x78,0x90,0xab,0xcd,0xe0});
    const auto result = etam::Bytes::from_nibbles(nibbles);
    BOOST_CHECK_EQUAL( result, expected_result );
}

BOOST_AUTO_TEST_SUITE_END()
