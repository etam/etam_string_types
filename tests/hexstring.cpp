#include <boost/test/unit_test.hpp>

#include <etam_types/hexstring.hpp>

#include <etam_types/bytes.hpp>
#include <etam_types/nibbles.hpp>

BOOST_AUTO_TEST_SUITE( hexstring )

BOOST_AUTO_TEST_CASE( from_bytes )
{
    const auto bytes = etam::Bytes{0x4d,0x61,0x6E};
    const auto expected_result = etam::HexString{"4d616e"};
    const auto result = etam::HexString::from_bytes(bytes);
    BOOST_CHECK_EQUAL( result, expected_result );
}

BOOST_AUTO_TEST_CASE( from_nibbles )
{
    const auto nibbles = etam::Nibbles{0x4,0xd,0x6,0x1,0x6,0xE};
    const auto expected_result = etam::HexString{"4d616e"};
    const auto result = etam::HexString::from_nibbles(nibbles);
    BOOST_CHECK_EQUAL( result, expected_result );
}

BOOST_AUTO_TEST_SUITE_END()
