#include <iostream>
#include <string>

#include <etam_types/base64string.hpp>
#include <etam_types/hexstring.hpp>

int main(int argc, char* argv[])
{
    std::string raw_hex;
    if (argc == 1) {
        std::cin >> raw_hex;
    } else if (argc == 2) {
        raw_hex = argv[1];
    } else {
        return 1;
    }
    std::cout << etam::Base64String::from_hex(raw_hex) << std::endl;
}
