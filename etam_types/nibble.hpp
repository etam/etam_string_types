#ifndef ETAM_NIBBLE_HPP
#define ETAM_NIBBLE_HPP

#include <stdexcept>

typedef unsigned char byte;

namespace etam {

class HexChar;

class Nibble
{
private:
    byte value = 0;

protected:
    static Nibble create_unchecked(const byte value)
    {
        auto nibble = Nibble{};
        nibble.value = value;
        return nibble;
    }

    static void throw_if_out_of_range(const byte value)
    {
        if (value > 0xf)
            throw std::out_of_range{"value too big for a nibble"};
    }

public:
    friend class Nibbles;

    Nibble() = default;
    Nibble(const Nibble&) = default;
    Nibble(Nibble&&) = default;

    Nibble(const byte new_value)
        : value(new_value)
    {
        throw_if_out_of_range(new_value);
    }

    static Nibble from_hex(const HexChar& hex);

    Nibble& operator=(const Nibble&) = default;
    Nibble& operator=(Nibble&&) = default;

    Nibble& operator=(const byte new_value)
    {
        throw_if_out_of_range(new_value);
        value = new_value;
        return *this;
    }

    operator byte() const
    {
        return value;
    }

    HexChar to_hex() const;
};

} // namespace etam

#endif // ETAM_NIBBLE_HPP
