#include "bytes.hpp"

#include <algorithm>
#include <stdexcept>

#include "base64string.hpp"
#include "hexstring.hpp"
#include "nibbles.hpp"

namespace etam {

Bytes Bytes::from_base64(const Base64String& base64)
{
    if (base64.size() == 0)
        return {};

    const auto base64_tail_early = base64.size() % 4;
    const auto base64_padding = (base64_tail_early != 0) ? 0 : std::count(base64.crbegin(), base64.crbegin() + 3, Base64Char::padding);
    const auto base64_tail = [base64_tail_early, base64_padding]()->std::size_t {
        if (base64_tail_early != 0)
            return base64_tail_early;
        if (base64_padding > 0)
            return 4 - base64_padding;
        return 0;
    }();
    const auto full_blocks = std::size_t{(base64.size() / 4) - !!base64_padding};
    const auto bytes_count = [base64_tail, full_blocks]()->std::size_t {
        auto bytes_count = full_blocks * 3;
        if (base64_tail != 0)
            bytes_count += base64_tail - 1;
        return bytes_count;
    }();

    auto bytes = Bytes(bytes_count);
    for (auto i = std::size_t{0}; i < full_blocks; ++i) {
        bytes[3*i] = (base64[4*i].to_byte() << 2) | (base64[4*i+1].to_byte() >> 4);
        bytes[3*i+1] = (base64[4*i+1].to_byte() << 4) | (base64[4*i+2].to_byte() >> 2);
        bytes[3*i+2] = (base64[4*i+2].to_byte() << 6) | base64[4*i+3].to_byte();
    }
    if (base64_tail == 0)
        return bytes;

    bytes[3*full_blocks] = (base64[4*full_blocks].to_byte() << 2) | (base64[4*full_blocks+1].to_byte() >> 4);
    if (base64_tail == 3)
        bytes[3*full_blocks+1] = (base64[4*full_blocks+1].to_byte() << 4) | (base64[4*full_blocks+2].to_byte() >> 2);
    return bytes;
}

Bytes Bytes::from_hex(const HexString& hex)
{
    const auto full_blocks = std::size_t{hex.size() / 2};
    const auto has_tail = bool{!!(hex.size() % 2)};
    const auto all_blocks = std::size_t{full_blocks + has_tail};
    auto bytes = Bytes(all_blocks);
    for (auto i = std::size_t{0}; i < full_blocks; ++i)
        bytes[i] = (hex[2*i].to_nibble() << 4) | hex[2*i+1].to_nibble();
    if (has_tail)
        bytes[full_blocks] = hex[2*full_blocks].to_nibble() << 4;
    return bytes;
}

Bytes Bytes::from_nibbles(const Nibbles& nibbles)
{
    const auto full_blocks = std::size_t{nibbles.size() / 2};
    const auto has_tail = bool{!!(nibbles.size() % 2)};
    const auto all_blocks = std::size_t{full_blocks + has_tail};
    auto bytes = Bytes(all_blocks);
    for (auto i = std::size_t{0}; i < full_blocks; ++i)
        bytes[i] = (nibbles[2*i] << 4) | nibbles[2*i+1];
    if (has_tail)
        bytes[full_blocks] = nibbles[2*full_blocks] << 4;
    return bytes;
}

Base64String Bytes::to_base64() const
{
    return Base64String::from_bytes(*this);
}

HexString Bytes::to_hex() const
{
    return HexString::from_bytes(*this);
}

Nibbles Bytes::to_nibbles() const
{
    return Nibbles::from_bytes(*this);
}

} // namespace etam
