#ifndef ETAM_BASE64CHAR_HPP
#define ETAM_BASE64CHAR_HPP

#include "detail/specialchar.hpp"
#include "detail/specialstringfwd.hpp"

typedef unsigned char byte;

namespace etam {

class Base64String;

class Base64Char
    : public detail::SpecialChar<Base64Char>
{
private:
    typedef detail::SpecialChar<Base64Char> BaseType;
    friend BaseType;

    friend detail::SpecialString<Base64String, Base64Char>;
    friend Base64String;

    static const Base64Char safe_default;

    static void throw_if_illegal(const char char_);

public:
    using BaseType::BaseType;

    static const Base64Char padding;

    static Base64Char from_byte(byte value);

    byte to_byte() const;
};

} // namespace etam

#endif // ETAM_BASE64CHAR_HPP
