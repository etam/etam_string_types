#include "nibbles.hpp"

#include <algorithm>

#include "base64string.hpp"
#include "bytes.hpp"
#include "hexstring.hpp"

namespace etam {

Nibbles Nibbles::from_bytes(const Bytes& bytes)
{
    const auto blocks = std::size_t{bytes.size()};
    auto nibbles = Nibbles(blocks * 2);
    for (auto i = std::size_t{0}; i < blocks; ++i) {
        nibbles[2*i] = bytes[i] >> 4;
        nibbles[2*i+1] = bytes[i] & 0xf;
    }
    return nibbles;
}

Nibbles Nibbles::from_hex(const HexString& hex)
{
    auto nibbles = Nibbles(hex.size());
    std::transform(hex.begin(), hex.end(), nibbles.begin(), Nibble::from_hex);
    return nibbles;
}

Nibbles Nibbles::from_string(const std::string& string)
{
    auto nibbles = Nibbles(string.size() * 2);
    for (auto i = std::size_t{0}; i < string.size(); ++i) {
        nibbles[2*i] = byte(string[i] >> 4);
        nibbles[2*i+1] = byte(string[i] & 0xf);
    }
    return nibbles;
}

std::string Nibbles::to_string() const
{
    if (value.size() % 2)
        throw std::invalid_argument{"nibbles length not even"};
    auto string = std::string(value.size()/2, '?');
    for (auto i = std::size_t{0}; i < string.size(); ++i)
        string[i] = (value[2*i] << 4) | value[2*i+1];
    return string;
}

Base64String Nibbles::to_base64() const
{
    return Base64String::from_nibbles(*this);
}

Bytes Nibbles::to_bytes() const
{
    return Bytes::from_nibbles(*this);
}

HexString Nibbles::to_hex() const
{
    return HexString::from_nibbles(*this);
}

} // namespace etam
