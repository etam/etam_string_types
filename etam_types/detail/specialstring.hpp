#ifndef ETAM_SPECIALSTRING_HPP
#define ETAM_SPECIALSTRING_HPP

#include <algorithm>
#include <cstring>
#include <experimental/string_view>
#include <istream>
#include <iterator>
#include <ostream>
#include <string>

#include "specialstringfwd.hpp"

namespace etam {
namespace detail {

template <class InheritingType, class SpecialCharType>
class SpecialString
{
protected:
    std::basic_string<SpecialCharType> value;

    auto begin()
    {
        return value.begin();
    }

    auto end()
    {
        return value.end();
    }

    auto rbegin()
    {
        return value.rbegin();
    }

    auto rend()
    {
        return value.rend();
    }

    auto& operator[](const std::size_t i)
    {
        return value[i];
    }

public:
    SpecialString() = default;
    SpecialString(const SpecialString&) = default;
    SpecialString(SpecialString&&) = default;

    SpecialString(const std::string& string)
        : SpecialString{std::experimental::string_view(string)}
    {}

    SpecialString(const char* const string)
        : SpecialString{std::experimental::string_view(string)}
    {}

    SpecialString(const std::experimental::string_view& string)
    {
        assign(string.begin(), string.end());
    }

    SpecialString(std::size_t size, SpecialCharType default_char)
        : value(size, default_char)
    {
        InheritingType::throw_if_illegal(value.begin(), value.end());
    }

    SpecialString& operator=(const SpecialString&) = default;
    SpecialString& operator=(SpecialString&&) = default;

    SpecialString& operator=(const std::string& string)
    {
        return operator=(std::experimental::string_view(string));
    }

    SpecialString& operator=(const char* const string)
    {
        return operator=(std::experimental::string_view(string));
    }

    SpecialString& operator=(const std::experimental::string_view& string)
    {
        return assign(string.begin(), string.end());
    }

    template <class InputIt>
    SpecialString& assign(const InputIt first, const InputIt last)
    {
        std::for_each(first, last, SpecialCharType::throw_if_illegal);
        InheritingType::throw_if_illegal(first, last);
        value.resize(std::distance(first, last), SpecialCharType::safe_default);
        std::transform(first, last, value.begin(), SpecialCharType::create_unchecked);
        return *this;
    }

    auto begin() const
    {
        return value.begin();
    }

    auto cbegin() const
    {
        return value.cbegin();
    }

    auto end() const
    {
        return value.end();
    }

    auto cend() const
    {
        return value.cend();
    }

    auto rbegin() const
    {
        return value.rbegin();
    }

    auto crbegin() const
    {
        return value.crbegin();
    }

    auto rend() const
    {
        return value.rend();
    }

    auto crend() const
    {
        return value.crend();
    }

    auto size() const
    {
        return value.size();
    }

    const auto& operator[](const std::size_t i) const
    {
        return value[i];
    }

    const char* to_cstring() const
    {
        return reinterpret_cast<const char*>(value.data());
    }

    std::experimental::string_view to_string_view() const
    {
        return {to_cstring(), value.size()};
    }

    template <class InheritingType_, class SpecialCharType_>
    friend bool operator==(const SpecialString<InheritingType_, SpecialCharType_>&, const SpecialString<InheritingType_, SpecialCharType_>&);

    template <class InheritingType_, class SpecialCharType_>
    friend bool operator!=(const SpecialString<InheritingType_, SpecialCharType_>&, const SpecialString<InheritingType_, SpecialCharType_>&);

    template <class InheritingType_, class SpecialCharType_>
    friend std::ostream& operator<<(std::ostream&, const SpecialString<InheritingType_, SpecialCharType_>&);
};

template <class InheritingType, class SpecialCharType>
bool operator==(const SpecialString<InheritingType, SpecialCharType>& a, const SpecialString<InheritingType, SpecialCharType>& b)
{
    return a.value == b.value;
}

template <class InheritingType, class SpecialCharType>
bool operator!=(const SpecialString<InheritingType, SpecialCharType>& a, const SpecialString<InheritingType, SpecialCharType>& b)
{
    return a.value != b.value;
}

template <class InheritingType, class SpecialCharType>
bool operator==(const SpecialString<InheritingType, SpecialCharType>& s1, const std::experimental::string_view& s2)
{
    return s1.size() == s2.size()
            && std::equal(s1.begin(), s1.end(), s2.begin());
}

template <class InheritingType, class SpecialCharType>
bool operator==(const std::experimental::string_view& s1, const SpecialString<InheritingType, SpecialCharType>& s2)
{
    return s2 == s1;
}

template <class InheritingType, class SpecialCharType>
std::ostream& operator<<(std::ostream& ostream, const SpecialString<InheritingType, SpecialCharType>& string)
{
    ostream << string.to_string_view();
    return ostream;
}

} // namespace detail
} // namespace etam

#endif // ETAM_SPECIALSTRING_HPP
