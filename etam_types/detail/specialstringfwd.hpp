#ifndef ETAM_SPECIALSTRINGFWD_HPP
#define ETAM_SPECIALSTRINGFWD_HPP

namespace etam {
namespace detail {

template <class InheritingType, class SpecialCharType>
class SpecialString;

} // namespace detail
} // namespace etam

#endif // ETAM_SPECIALSTRINGFWD_HPP
