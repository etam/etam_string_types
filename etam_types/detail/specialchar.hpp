#ifndef ETAM_SPECIALCHAR_HPP
#define ETAM_SPECIALCHAR_HPP

#include "specialstringfwd.hpp"

namespace etam {
namespace detail {

template <class InheritingType>
class SpecialChar
{
protected:
    char char_;

    static InheritingType create_unchecked(const char char_)
    {
        auto c = InheritingType{};
        c.char_ = char_;
        return c;
    }

    static void throw_if_illegal(const char char_)
    {
        if (char_ == '\0')
            return;
        InheritingType::throw_if_illegal(char_);
    }

public:
    SpecialChar() = default;
    SpecialChar(const SpecialChar&) = default;
    SpecialChar(SpecialChar&&) = default;

    SpecialChar(const char char__)
        : char_(char__)
    {
        throw_if_illegal(char__);
    }

    SpecialChar& operator=(const SpecialChar&) = default;
    SpecialChar& operator=(SpecialChar&&) = default;

    SpecialChar& operator=(const char char__)
    {
        throw_if_illegal(char__);
        char_ = char__;
        return *this;
    }

    operator char() const
    {
        return char_;
    }
};

} // namespace detail
} // namespace etam

#endif // ETAM_SPECIALCHAR_HPP
