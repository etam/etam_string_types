#ifndef ETAM_HEXCHAR_HPP
#define ETAM_HEXCHAR_HPP

#include "detail/specialchar.hpp"
#include "detail/specialstringfwd.hpp"

namespace etam {

class HexString;
class Nibble;

class HexChar
    : public detail::SpecialChar<HexChar>
{
private:
    typedef detail::SpecialChar<HexChar> BaseType;
    friend BaseType;

    friend detail::SpecialString<HexString, HexChar>;
    friend HexString;

    static const HexChar safe_default;

    static void throw_if_illegal(const char char_);

public:
    using BaseType::BaseType;

    static HexChar from_nibble(const Nibble& nibble);

    Nibble to_nibble() const;
};

} // namespace etam

#endif // ETAM_HEXCHAR_HPP
