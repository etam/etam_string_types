#include "nibble.hpp"

#include "hexchar.hpp"

namespace etam {

Nibble Nibble::from_hex(const HexChar& hex)
{
    if (hex >= 'a' && hex <= 'f')
        return create_unchecked(hex - 'a' + 10);
    if (hex >= 'A' && hex <= 'F')
        return create_unchecked(hex - 'A' + 10);
    return create_unchecked(hex - '0');
}

HexChar Nibble::to_hex() const
{
    return HexChar::from_nibble(*this);
}

} // namespace etam
