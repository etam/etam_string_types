#ifndef ETAM_BASE64STRING_HPP
#define ETAM_BASE64STRING_HPP

#include <algorithm>
#include <iterator>
#include <stdexcept>

#include "base64char.hpp"
#include "detail/specialstring.hpp"

namespace etam {

class Bytes;
class HexString;
class Nibbles;

namespace detail {

template <class BidirIt, class Size, class T>
BidirIt omit_end_n(BidirIt last, const Size n, const T& value)
{
    for (Size i = 0; i < n; ++i) {
        BidirIt probably_last = last - 1;
        if (*probably_last == value)
            last = probably_last;
        else
            break;
    }
    return last;
}

template <class T>
auto equal_to(const T& lhs)
{
    return [&lhs](const auto& rhs) {
        return lhs == rhs;
    };
}

} // namespace detail


class Base64String
    : public detail::SpecialString<Base64String, Base64Char>
{
private:
    typedef detail::SpecialString<Base64String, Base64Char> BaseType;
    friend BaseType;

    template <class BidirIt>
    static void throw_if_illegal(const BidirIt first, const BidirIt last)
    {
        const auto length = std::distance(first, last);
        if (length == 0)
            return;
        const auto tail_length = length % 4;
        if (tail_length == 1)
            throw std::invalid_argument{"invalid length"};
        const auto before_padding_last = (tail_length == 0)
                                            ? omit_end_n(last, 2, Base64Char::padding)
                                            : last;
        if (std::any_of(first, before_padding_last, equal_to(Base64Char::padding)))
            throw std::invalid_argument{"invalid padding character"};
    }

public:
    using BaseType::BaseType;

    static Base64String from_bytes(const Bytes& bytes);
    static Base64String from_hex(const HexString& hex);
    static Base64String from_nibbles(const Nibbles& nibbles);

    Bytes to_bytes() const;
    HexString to_hex() const;
    Nibbles to_nibbles() const;
};

} // namespace etam

#endif // ETAM_BASE64STRING_HPP
