#include "base64string.hpp"

#include "bytes.hpp"
#include "hexstring.hpp"
#include "nibbles.hpp"

namespace etam {

Base64String Base64String::from_bytes(const Bytes& bytes)
{
    const auto full_blocks = std::size_t{bytes.size() / 3};
    const auto bytes_tail = std::size_t{bytes.size() % 3};
    const auto all_blocks = std::size_t{full_blocks + !!bytes_tail};

    auto base64 = Base64String(all_blocks*4, Base64Char::safe_default);

    for (auto i = std::size_t{0}; i < full_blocks; ++i) {
        base64[4*i] = Base64Char::from_byte(bytes[3*i] >> 2);
        base64[4*i+1] = Base64Char::from_byte(((bytes[3*i] & 0x3) << 4) | (bytes[3*i+1] >> 4));
        base64[4*i+2] = Base64Char::from_byte(((bytes[3*i+1] & 0xf) << 2) | (bytes[3*i+2] >> 6));
        base64[4*i+3] = Base64Char::from_byte(bytes[3*i+2] & 0x3f);
    }
    if (bytes_tail == 0)
        return base64;

    base64[4*full_blocks] = Base64Char::from_byte(bytes[3*full_blocks] >> 2);
    if (bytes_tail == 1) {
        base64[4*full_blocks+1] = Base64Char::from_byte((bytes[3*full_blocks] & 0x3) << 4);
        base64[4*full_blocks+2] = Base64Char::padding;
    } else if (bytes_tail == 2) {
        base64[4*full_blocks+1] = Base64Char::from_byte(((bytes[3*full_blocks] & 0x3) << 4) | (bytes[3*full_blocks+1] >> 4));
        base64[4*full_blocks+2] = Base64Char::from_byte((bytes[3*full_blocks+1] & 0xf) << 2);
    }
    base64[4*full_blocks+3] = Base64Char::padding;
    return base64;
}

Base64String Base64String::from_hex(const HexString& hex)
{
    return Base64String::from_bytes(Bytes::from_hex(hex));
}

Base64String Base64String::from_nibbles(const Nibbles& nibbles)
{
    if (nibbles.size() % 3)
        throw std::logic_error{"nibbles size is not multiple of 3"};
    const auto groups = std::size_t{nibbles.size() / 3};
    auto base64 = Base64String(groups*2, Base64Char::safe_default);
    for (auto i = std::size_t{0}; i < groups; ++i) {
        base64[i*2] = Base64Char::from_byte((nibbles[i*3] << 2) | (nibbles[i*3+1] >> 2));
        base64[i*2+1] = Base64Char::from_byte(((nibbles[i*3+1] & 0x3) << 4) | nibbles[i*3+2]);
    }
    return base64;
}

Bytes Base64String::to_bytes() const
{
    return Bytes::from_base64(*this);
}

HexString Base64String::to_hex() const
{
    return HexString::from_bytes(to_bytes());
}

Nibbles Base64String::to_nibbles() const
{
    return Nibbles::from_bytes(to_bytes());
}

} // namespace etam
