#ifndef ETAM_HEXSTRING_HPP
#define ETAM_HEXSTRING_HPP

#include "detail/specialstring.hpp"
#include "hexchar.hpp"

namespace etam {

class Base64String;
class Bytes;
class Nibbles;

class HexString
    : public detail::SpecialString<HexString, HexChar>
{
private:
    typedef detail::SpecialString<HexString, HexChar> BaseType;
    friend BaseType;

    template <class CharIt>
    static void throw_if_illegal(CharIt /*first*/, CharIt /*last*/)
    {}

public:
    using BaseType::BaseType;

    static HexString from_base64(const Base64String& base64);
    static HexString from_bytes(const Bytes& bytes);
    static HexString from_nibbles(const Nibbles& nibbles);

    Base64String to_base64() const;
    Bytes to_bytes() const;
    Nibbles to_nibbles() const;
};

} // namespace etam

#endif // ETAM_HEXSTRING_HPP
