#include "hexstring.hpp"

#include <algorithm>

#include "base64string.hpp"
#include "bytes.hpp"
#include "nibbles.hpp"

namespace etam {

HexString HexString::from_base64(const Base64String& base64)
{
    return from_bytes(base64.to_bytes());
}

HexString HexString::from_bytes(const Bytes& bytes)
{
    const auto blocks = std::size_t{bytes.size()};
    auto hex = HexString(blocks*2, HexChar::safe_default);
    for (auto i = std::size_t{0}; i < blocks; ++i) {
        hex[2*i] = HexChar::from_nibble(bytes[i] >> 4);
        hex[2*i+1] = HexChar::from_nibble(bytes[i] & 0xf);
    }
    return hex;
}

HexString HexString::from_nibbles(const Nibbles& nibbles)
{
    auto hex = HexString(nibbles.size(), HexChar::safe_default);
    std::transform(nibbles.begin(), nibbles.end(), hex.begin(), HexChar::from_nibble);
    return hex;
}

Base64String HexString::to_base64() const
{
    return Base64String::from_hex(*this);
}

Bytes HexString::to_bytes() const
{
    return Bytes::from_hex(*this);
}

Nibbles HexString::to_nibbles() const
{
    return Nibbles::from_hex(*this);
}

} // namespace etam
