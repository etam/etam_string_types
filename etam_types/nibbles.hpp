#ifndef ETAM_NIBBLES_HPP
#define ETAM_NIBBLES_HPP

#include <initializer_list>
#include <string>
#include <vector>

#include "nibble.hpp"

namespace etam {

class Base64String;
class Bytes;
class HexString;

class Nibbles
{
private:
    std::vector<Nibble> value;

public:
    Nibbles() = default;
    Nibbles(const Nibbles&) = default;
    Nibbles(Nibbles&&) = default;

    explicit Nibbles(std::size_t size)
        : value(size)
    {}

    Nibbles(std::initializer_list<Nibble> list)
        : value(list)
    {}

    static Nibbles from_bytes(const Bytes& bytes);
    static Nibbles from_hex(const HexString& hex);
    static Nibbles from_string(const std::string& string);

    Nibbles& operator=(const Nibbles&) = default;
    Nibbles& operator=(Nibbles&&) = default;

    Nibbles& operator=(std::initializer_list<Nibble> list)
    {
        value = list;
        return *this;
    }

    std::string to_string() const;

    auto begin()
    {
        return value.begin();
    }

    auto begin() const
    {
        return value.begin();
    }

    auto cbegin() const
    {
        return value.cbegin();
    }

    auto end()
    {
        return value.end();
    }

    auto end() const
    {
        return value.end();
    }

    auto cend() const
    {
        return value.cend();
    }

    auto size() const
    {
        return value.size();
    }

    decltype(auto) operator[](const std::size_t i)
    {
        return value[i];
    }

    decltype(auto) operator[](const std::size_t i) const
    {
        return value[i];
    }

    friend bool operator==(const Nibbles&, const Nibbles&);
    friend bool operator!=(const Nibbles&, const Nibbles&);

    Base64String to_base64() const;
    Bytes to_bytes() const;
    HexString to_hex() const;
};

inline bool operator==(const Nibbles& a, const Nibbles& b)
{
    return a.value == b.value;
}

inline bool operator!=(const Nibbles& a, const Nibbles& b)
{
    return a.value != b.value;
}

} // namespace etam

#endif // ETAM_NIBBLES_HPP
