#include "hexchar.hpp"

#include <stdexcept>

#include "nibble.hpp"

namespace etam {

const HexChar HexChar::safe_default{'a'};

void HexChar::throw_if_illegal(const char char_)
{
    if (   (char_ >= '0' && char_ <= '9')
        || (char_ >= 'a' && char_ <= 'f')
        || (char_ >= 'A' && char_ <= 'F'))
        return;
    throw std::invalid_argument{"invalid hex char"};
}

HexChar HexChar::from_nibble(const Nibble& nibble)
{
    if (nibble < 10)
        return create_unchecked('0' + nibble);
    return create_unchecked('a' + nibble - 10);
}

Nibble HexChar::to_nibble() const
{
    return Nibble::from_hex(*this);
}

} // namespace etam
