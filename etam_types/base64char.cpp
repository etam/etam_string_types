#include "base64char.hpp"

#include <stdexcept>

namespace etam {

const Base64Char Base64Char::safe_default{'a'};
const Base64Char Base64Char::padding{'='};

void Base64Char::throw_if_illegal(const char char_)
{
    if (   (char_ >= 'A' && char_ <= 'Z')
        || (char_ >= 'a' && char_ <= 'z')
        || (char_ >= '0' && char_ <= '9')
        ||  char_ == '+'
        ||  char_ == '/'
        ||  char_ == padding
       )
        return;
    throw std::invalid_argument{"invalid base64 char"};
}

Base64Char Base64Char::from_byte(const byte value)
{
    if (value > 63)
        throw std::out_of_range{"byte value out of range"};
    if (value <= 25)
        return create_unchecked(value + 'A');
    if (value <= 51)
        return create_unchecked(value-26 + 'a');
    if (value <= 61)
        return create_unchecked(value-52 + '0');
    if (value == 62)
        return create_unchecked('+');
    return create_unchecked('/');
}

byte Base64Char::to_byte() const
{
    if (char_ >= 'A' && char_ <= 'Z')
        return char_ - 'A';
    if (char_ >= 'a' && char_ <= 'z')
        return char_ - 'a' + 26;
    if (char_ >= '0' && char_ <='9')
        return char_ - '0' + 52;
    if (char_ == '+')
        return 62;
    if (char_ == '/')
        return 63;
    return 0;
}

} // namespace etam
