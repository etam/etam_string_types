#ifndef ETAM_BYTES_HPP
#define ETAM_BYTES_HPP

#include <vector>

typedef unsigned char byte;

namespace etam {

class Base64String;
class HexString;
class Nibbles;

class Bytes
    : public std::vector<byte>
{
    typedef std::vector<byte> BaseType;
public:
    using BaseType::BaseType;

    static Bytes from_base64(const Base64String& base64);
    static Bytes from_hex(const HexString& hex);
    static Bytes from_nibbles(const Nibbles& nibbles);

    Base64String to_base64() const;
    HexString to_hex() const;
    Nibbles to_nibbles() const;
};

} // namespace etam

#endif // ETAM_BYTES_HPP
